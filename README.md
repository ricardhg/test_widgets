
# widget drag&drop test
by Ricard Hernàndez
ricard.hernandez.gi@gmail.com
## Application
The applicacion allows the user to create a set of screens and place a random selection of predefined widgets on each screen. User can add or remove a widget to any screen using drag and drop.

There are three main areas on the screen:
* Left sidebar
* Center area
* Right sidebar
### Left sidebar
Shows all screens, allowing the user to:
* Add new screens
* Choose current screen
* Delete all screens
* Save to LocalStorage the current screen set
* Restore screen set from LocalStorage
### Center Area or Current Screen content
Is the area where current screen widgets are shown. Widgets can be dragged in from widget table and also sent back to it.
The titlle bar shows the screen name and allows two functions:
* delete the screen
* empty the screen, sending all widgets to widget table
### Right sidebar or Widget table
Shows a predefined list of available widgets that can be moved to/from center area.
The title bar allows to add all widgets to current screen.

## Technology
The application has been created using the following techniques and tools:

* All the application is made using React JS
* The scaffolding has been made with a custom webpack configuration
* Boostrap & reactstrap have been used to add a responsive behaviour to the app
* Icons used are from FontAwesome 4.7 library
* Drag&Drop has been programmed without third party libraries
* Components css have been applied through styled-components library

Package.json dependencies:
```
"dependencies": {
	"bootstrap": "^4.4.1",
	"font-awesome": "^4.7.0",
	"react": "^16.13.1",
	"react-dom": "^16.13.1",
	"reactstrap": "^8.4.1",
	"styled-components": "^5.0.1"
	},
```

  
## To-Do
Some parts of the project should be finished and/or improved
* Create some testing functions
* Ask for confirmation and show some feedback on scren operations like deleting, saving or restoring to localstorage
## How to test the application
Clone project to local computer, open a terminal/command shell, enter the application base folder (where package.json is) and install dependencies with npm/yarn:
```
npm install
```


Once dependencies are installed, start the application with:
```
npm run start
```
Note: NodeJS and NPM or Yarn package managers are required
