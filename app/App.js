import React, { useState } from 'react';
import { Container, Row, Col } from 'reactstrap';
import SideBar from './components/Sidebar';
import MainArea from './components/ScreenContent';
import Header from './components/Header';

import {SpacerV} from './helpers/helpers';
import {PRIMARY, SUCCESS, DANGER, WARNING, INFO} from './helpers/widget-colors';

const WIDGETS = [
    { name: 'Bicycle', color: WARNING },
    { name: 'Subway', color: INFO },
    { name: 'Motorcycle', color: SUCCESS },
    { name: 'Plane', color: DANGER },
    { name: 'Car', color: PRIMARY },
    { name: 'Ship', color: WARNING },
    { name: 'Cab', color: PRIMARY },
    { name: 'Bus', color: SUCCESS },
]

/**
 * APP 
 * Shows a set of screens that can be populated with drag&drop widgets 
 * Allows save&load of screens set to localStorage
 * Responsive design using Bootstrap grid layout
 * Components used:
 *      SideBar: shows set of screens, allows to add new screens, save, load and clear set 
 *      MainArea: allows to populate current screen with widgets
 * 
 * @author: ricard.hernandez.gi@gmail.com
 */
export default () => {

    //returns a new screen item, clear is used to force index to 0
    //each screen receives a copy of the WIDGETS array, initially set to 'available' property
    //screen names are set to "Screen X"
    const newScreen = (clear, name) => { return { 
            name: name ? name : `Screen ${!clear && screens ? screens.length : 0}`,
            selected: [],
            available: WIDGETS 
        } };
    
    //hooks used, screens is initialized to a newScreen()
    const [screens, setScreens] = useState([newScreen()]);
    const [current, setCurrent] = useState(0);
    
    //utility methods to clear screens, save & load from localStorage 
    //const clearScreens = () => {setScreens([newScreen(true)]); setCurrent(0)};
    const clearScreens = () => {setScreens([]); setCurrent(0)};
    const saveScreens = () => localStorage.setItem('bigfinite_test_screens', JSON.stringify(screens));
    const loadScreens = () => setScreens(JSON.parse(localStorage.getItem('bigfinite_test_screens')));
    
    //selects all widgets
    const allSelected = () => {
        let newScreenSet = JSON.parse(JSON.stringify(screens));
        newScreenSet[current].selected = [ ...newScreenSet[current].selected, ...newScreenSet[current].available];
        newScreenSet[current].available = [];
        setScreens(newScreenSet);
    }
    //removes all selected widgets
    const allAvailable = () => {
        let newScreenSet = JSON.parse(JSON.stringify(screens));
        newScreenSet[current].available = [ ...newScreenSet[current].available, ...newScreenSet[current].selected];
        newScreenSet[current].selected = [];
        setScreens(newScreenSet);
    }
    //removes current screen from set
    const deleteCurrentScreen = () => {
        let newScreenSet = JSON.parse(JSON.stringify(screens));
        newScreenSet.splice(current,1);
        setScreens(newScreenSet);
        setCurrent(0);
    }
    
    //set of props sent to components
    const sideBarProps = { screens, setScreens, current, setCurrent, newScreen, clearScreens, saveScreens, loadScreens };
    const mainAreaProps = { screens, setScreens, current, allSelected, allAvailable, deleteCurrentScreen };
    
    return (
        <>
            <Header />
            <SpacerV />
            <Container >
                <Row>
                    <Col md="3">
                        <SideBar {...sideBarProps} />
                        <SpacerV height="40px" />
                    </Col>
                    <Col>
                        <MainArea {...mainAreaProps} />
                    </Col>
                </Row>
            </Container>
        </>
    );
}