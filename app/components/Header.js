import React from 'react';
import styled from 'styled-components';
import {Container} from 'reactstrap';

import logo from '../img/bigfinite-logo.svg';

const HeadDiv = styled.div`
    background-color: #0077BF;
    position: fixed;
    top: 0;
    width: 100%;
    box-shadow: 0 -6px 49px 0 rgba(0,0,0,0.5);
    height: 70px;
    transition: height 0.3s;
    -ms-flex-align: center;
    align-items: center;
    z-index: 100;
    padding: 15px 0;
`;

export default () => <HeadDiv><Container><img src={logo} /></Container></HeadDiv>;