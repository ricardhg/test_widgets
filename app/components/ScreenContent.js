import React, { useState, useRef } from 'react';
import { Row, Col } from 'reactstrap';
import styled from 'styled-components';

import {Title, FunctionIcon, SpacerH} from '../helpers/helpers';

const WidgetItem = styled.div`
font-size: 1.2em;
text-align: center;
color: palevioletred;
padding: 10px;
border: 1px solid;
margin-bottom: ${props => props.mb || "10px"};
border-radius: .25rem;
border-color:${props => props.color.border || "black"};
background-color:${props => props.color.back || "white"};
color:${props => props.color.font || "black"};
`;

const DragArea = styled.div`
width: "100%";
min-height: 500px;
`;

/**
 * ScreenContent component shows current screen in two areas:
 *  selected area: shows widgets assigned to screen
 *  available area: shows widgets available to assign
 * widgets can be dragged to&from each area 
 * 
 * received props:
 *   screens: whole set of screens
 *   setScreens: hook used to change app screen set when any widget is moved
 *   current: index of current screen
 * 
 */
export default ({ screens, setScreens, current, allSelected, allAvailable, deleteCurrentScreen }) => {

    //hoooks used: dragging shows if a widget is being dragged
    //draggingOverWidget shows if a widget is over another, in the same or different area 
    const [dragging, setDragging] = useState(false);
    const [draggingOverWidget, setDraggingOverWidget] = useState(false);

    //ref used to hold drag widget data
    const dragItem = useRef();
    //ref used to hold drag widget element
    const dragItemNode = useRef();

    const currentScreen = screens[current];

    //dragStart sets current dragItemNode and its data
    //also sets dragend event handler and dragging state to true
    const handleDragStart = (e, item) => {
        dragItemNode.current = e.target;
        dragItemNode.current.addEventListener('dragend', handleDragEnd)
        dragItem.current = item;

        setTimeout(() => {
            setDragging(true);
        }, 0)
    }

    //handleDragEnter fires when dragged widget enters a draggable area different than itself
    //recevies targetItem with is an object with {area, index} of the "destiny" widget or area 
    const handleDragEnter = (e, targetItem) => {
        if (dragItemNode.current !== e.target) {
            //set to true to distinguish when dragging over draggable area or widget
            setDraggingOverWidget(true);

            //when dragging over item, screens are changed using seScreens hook
            //function receives previous state (oldScreenSet)
            setScreens(oldScreenSet => {
                //creating a deep copy of old list
                let newScreenSet = JSON.parse(JSON.stringify(oldScreenSet));
                //item being dragged is "cut" from its origin area through splice(index,1) and "pasted" into destiny area through splice(index,0,item)   
                //origin & destiny area can be the same
                newScreenSet[current][targetItem.area].splice(targetItem.index, 0, newScreenSet[current][dragItem.current.area].splice(dragItem.current.index, 1)[0]);
                //current item ref is set to its new location (targetItem)
                dragItem.current = targetItem;
                //newScreenSet is returned to setScreens hook
                return newScreenSet
            })
        } 
    }

    //resetting controls & removing event listener when drag operation ends  
    const handleDragEnd = (e) => {
        setDragging(false);
        dragItem.current = null;
        setDraggingOverWidget(false);
        dragItemNode.current.removeEventListener('dragend', handleDragEnd)
        dragItemNode.current = null;
    }

    //selectedItems: array of bootstrap Cols with one WidgetItem each
    //array is created from 'selected' property of current screen 
    //each WidgetItem shows name & icon and sets dragging events
    //handleDragStart & handleDragEnter receive event and current item {area,index}
    //area is set to 'selected'
    //icon name is set from 'name' property of widget
    const selectedItems = !currentScreen ? null : currentScreen.selected.map((el, index) =>
        <Col xs="6"  md="4" key={index} >
            <WidgetItem 
                draggable
                onDragLeave={(e)=> setDraggingOverWidget(false)}
                onDragStart={(e) => handleDragStart(e, { area: 'selected', index })}
                onDragEnter={dragging ? (e) => { handleDragEnter(e, { area: 'selected', index }) } : null}
                color={el.color}
                mb="30px" 
            >
                <i className={`fa fa-${el.name.toLowerCase()} fa-3x`} /><br />
                {el.name}
            </WidgetItem>
        </Col>);

    //same as selectedItems but from 'available' property of current screen
    //WidgetItem shows only icon
    //area is set to 'available'
    const availableItems = !currentScreen ? null : currentScreen.available.map((el, index) =>
        <WidgetItem
            key={index} 
            draggable
            onDragLeave={(e)=> setDraggingOverWidget(false)}
            onDragStart={(e) => handleDragStart(e, { area: 'available', index })}
            onDragEnter={dragging ? (e) => { handleDragEnter(e, { area: 'available', index }) } : null}
            color ={el.color}
        >
            <i className={`fa fa-${el.name.toLowerCase()} fa-2x`} />
        </WidgetItem>);

    //responsive content is shown in two DragArea components
    //left dragArea shows selected widgets
    //right dragArea shows available widgets 
    //dragArea is also a "draggable" zone where widgets can be dropped only if:
    //    there is a widget being dragged
    //    widget is not over another widget
    //    widget being dragged comes from the other area
    //in this case, dropped widget index is set to last of that area
    //otherwise dragEnter is set to null
    return (
        <Row>
            <Col>
                <Title >
                
                    {currentScreen ? 
                        <>
                            <FunctionIcon style={{color:"red"}} onClick={deleteCurrentScreen} className="fa fa-times " />
                            <SpacerH />
                            {currentScreen.name}
                            <span className="float-right">
                                <FunctionIcon onClick={allAvailable} className="fa fa-angle-double-right " />
                            </span>
                        </>
                        : "Screeen" }
                    
                </Title>
                <DragArea onDragEnter={dragging && !draggingOverWidget && dragItem.current.area!=='selected' ? (e) => handleDragEnter(e, { area: "selected", index:  currentScreen.selected.length}) : null} >
                    <Row>
                        {selectedItems}
                    </Row>
                </DragArea>
            </Col>
            <Col xs="4" md="3">
                <Title >
                    Widgets
                    {!screens.length ? null :
                        <span className="float-right">
                            <FunctionIcon onClick={allSelected} className="fa fa-angle-double-left " />
                        </span>
                    }   
                </Title>
                <DragArea onDragEnter={dragging && !draggingOverWidget && dragItem.current.area!=='available' ? (e) => handleDragEnter(e, { area: "available", index:  currentScreen.available.length }) : null} >
                    {availableItems}
                </DragArea>
            </Col>
        </Row>
    );
}





