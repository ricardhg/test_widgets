import React, {useState} from 'react';
import { Button, Row, Col, Badge, InputGroup, InputGroupAddon, InputGroupText, Input } from 'reactstrap';

import { Title, SpacerH, FunctionIcon } from '../helpers/helpers';



export default ({ screens, setScreens, current, setCurrent, newScreen, clearScreens, saveScreens, loadScreens }) => {

    const [name, setName] = useState("");

    //buttons showing scren name and widget items
    //onClick sets current screen
    //responsive: on small screen shows
    const screenItems = screens.map((el, index) =>
        <Col xs="6" sm="4" md="12" key={index} >
            <Button
                className="mb-2"
                color={(index === current) ? "secondary" : "light"}
                block
                onClick={() => setCurrent(index)}>
                {el.name}<SpacerH />{el.selected.length ? <Badge color="danger" pill>{el.selected.length}</Badge> : null}
            </Button>
        </Col>
    );

    //method called from onCLick | onKeyDown
    //should be processed in a single batch by react,
    //thus current is set to screens.length instead of screens.lenght-1
    const addScreenBatch = () => {
        setScreens([...screens, newScreen(false, name)]); 
        setCurrent(screens.length); 
        setName('');
    }

    //if enter is pressed inside input field & field has content Add method is fired
    const keyPress = (e)=>{
        if(e.keyCode == 13 && name.length){
            addScreenBatch();
        }
     }



    return (
        <>
            <Title>
                Screens
                <span className="float-right">
                    <FunctionIcon onClick={clearScreens} className="fa fa-trash mr-2" />
                    <FunctionIcon onClick={saveScreens} className="fa fa-floppy-o mr-2" />
                    <FunctionIcon onClick={loadScreens} className="fa fa-upload " />
                </span>
            </Title>
            <Row>
                {screenItems}
            </Row>
            <Row>
                <Col xs="12" sm="8" md="12" >
                    <InputGroup>
                        <InputGroupAddon style={{cursor: "pointer"}} addonType="prepend" onClick={() => addScreenBatch()}>
                            <InputGroupText >Add</InputGroupText>
                        </InputGroupAddon>
                        <Input 
                            placeholder="screen name" 
                            value={name}
                            onChange={e => setName(e.target.value)}
                            onKeyDown={e => keyPress(e)}
                        />
                    </InputGroup>
                </Col>
                


            </Row>
        </>
    );

}