import styled from 'styled-components';

//Common styled components
const BIGBLUECOLOR = '#0077BF';

export const Title = styled.h2`
    font-size: 1.2em;
    color: ${BIGBLUECOLOR};
    border-bottom: 1px solid ${BIGBLUECOLOR};
    margin-bottom: 15px;
    padding-bottom: 7px;
    width: 100%;
`;

export const SpacerV = styled.div`
        height: ${props => props.height || "100px"};
`;

export const SpacerH = styled.div`
    width: ${props => props.width || "15px"};
    display: inline-block;
`;


//used in function icons: trash, save, load...
export const FunctionIcon = styled.i`
    cursor:pointer;
`;