
export const PRIMARY = { back: '#cce5ff', font: '#004085', border: '#b8daff' };
export const SUCCESS = { back: '#d4edda', font: '#155724', border: '#c3e6cb' };
export const DANGER = { back: '#f8d7da', font: '#721c24', border: '#f5c6cb' };
export const WARNING = { back: '#fff3cd', font: '#856404', border: '#ffeeba' };
export const INFO = { back: '#d1ecf1', font: '#0c5460', border: '#bee5eb' };


